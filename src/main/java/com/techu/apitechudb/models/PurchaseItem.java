package com.techu.apitechudb.models;

public class PurchaseItem {

    private String idProd; //Id del producto
    private float quantity; //Cantidad comprada

    public PurchaseItem() {
    }

    public PurchaseItem(String idProd, float quantity) {
        this.idProd = idProd;
        this.quantity = quantity;
    }

    public void setIdProd(String idProd) {
        this.idProd = idProd;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public String getIdProd() {
        return idProd;
    }

    public float getQuantity() {
        return quantity;
    }
}
