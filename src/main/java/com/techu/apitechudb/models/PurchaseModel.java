package com.techu.apitechudb.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PurchaseModel {

    private String id;     //Identificador de la compra
    private String userid; //Usuario que realizaa la compra
    private float amount;  //Precio total de la compra
    private ArrayList<PurchaseItem> purchaseItems; //Lista de items comprados

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userid, float amount, ArrayList<PurchaseItem> purchaseItems) {
        this.id = id;
        this.userid = userid;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setPurchaseItems(ArrayList<PurchaseItem> purchaseItems){
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public String getUserid() {
        return userid;
    }

    public float getAmount() {
        return amount;
    }

    public ArrayList<PurchaseItem> getPurchaseItems() {
        return purchaseItems;
    }
}
