package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseItem;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.SQLOutput;
import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    public List<PurchaseModel> findAll (){
        System.out.println("findAll de PurchaseService");

        return purchaseRepository.findAll();
    }

    public ErrorService add(PurchaseModel purchase){
        System.out.println("add en PurchaseService");
//Validamos id del usuario
        System.out.println("Validamos si existe el usuario: " + purchase.getUserid());
        Optional<UserModel> user = userService.findById(purchase.getUserid());

        if (!user.isPresent()) {
            return ErrorService.USER_NOT_FOUND;
        }
//Validamos que no exista ya una compra con el mismo ID
        System.out.println("Validamos que no exista una compra con el mismo ID");
        System.out.println("Id de la compra: " + purchase.getId());

        Optional<PurchaseModel> purchaseaux = this.findById(purchase.getId());

        if (purchaseaux.isPresent()){
            return ErrorService.PURCHASE_EXIST;
        }
//Calculamos el total de la compra y lo actualizamos en el item a guardar
        System.out.println("Calculando el precio total");

        float total = 0;

        for (PurchaseItem item:purchase.getPurchaseItems()) {
            Optional<ProductModel> result = productService.findById(item.getIdProd());
            if (result.isPresent()) {
                float precio = result.get().getPrice();
                total += (precio * item.getQuantity());
            } else {
                return ErrorService.PRODUCT_NOT_FOUND;
            }
        }

        purchase.setAmount(total);
        this.purchaseRepository.save(purchase);
        return ErrorService.OK;
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById en PurchaseService");

        return this.purchaseRepository.findById(id);
    }


}
