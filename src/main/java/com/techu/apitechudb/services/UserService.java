package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAll en UserService");

        return this.userRepository.findAll();
    }

    public Optional<List<UserModel>> findByAge(int age){
        System.out.println("findByAge en userService ");

//        Versión  mia en la que implementaba findByAge en UserRepositiry
//        return this.userRepository.findByAge(age);
//        System.out.println("findByAge en userRepository ");

        ArrayList<UserModel> lista = new ArrayList<>();
        boolean hayclientes = false;

        for(UserModel userInList: userRepository.findAll()){
            if (userInList.getAge() == age) {
                System.out.println("Un usuario encontrado");
                lista.add(userInList);
                hayclientes = true;
            }
        }

        Optional<List<UserModel>> result = Optional.empty();

        if (hayclientes == true) {
            result = Optional.of(lista);
        }

        return result;

    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService ");

        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user){
        System.out.println("add en UserModel");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel userModel){
        System.out.println("update de UserService");

        return this.userRepository.update(userModel);
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");

        boolean result = false;

        Optional <UserModel> userToDelete = this.findById(id);

        if (userToDelete.isPresent() == true ) {
            result = true;
            this.userRepository.delete(userToDelete.get());
        }

        return result;
    }
}
