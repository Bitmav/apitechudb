package com.techu.apitechudb.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloControler {

    @RequestMapping("/")
    public String index(){
        return "Hola mundo desde API TECH U V2";
    }

    @RequestMapping("/hello")
    public String hello (@RequestParam(value="name", defaultValue = "Tech U") String name) {
        return String.format("Hola %s" ,name);
    }
}
