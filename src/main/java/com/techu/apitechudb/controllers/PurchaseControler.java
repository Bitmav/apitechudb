package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseItem;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ErrorService;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("apitechu/v2")
public class PurchaseControler {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<Object> getPurchases() {
        System.out.println("getPurchases de PurchaseControlers");

        return new ResponseEntity<>(purchaseService.findAll(),HttpStatus.OK);
    }

    @PostMapping("/purchases")
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase de PurchaseController");

        ErrorService result = purchaseService.add(purchase);

        switch (result){
            case OK:
                return new ResponseEntity<>(purchaseService.findById(purchase.getId()),HttpStatus.CREATED);
            case PURCHASE_EXIST:
                return new ResponseEntity<>("La compra ya existe",HttpStatus.BAD_REQUEST);
            case USER_NOT_FOUND:
                return new ResponseEntity<>("Comprador no existe",HttpStatus.BAD_REQUEST);
            case PRODUCT_NOT_FOUND:
                return new ResponseEntity<>("Producto no existe",HttpStatus.BAD_REQUEST);
            default:
                return new ResponseEntity<>("No se ha podido realizar la operacióm",HttpStatus.BAD_REQUEST);
        }

    }

}
