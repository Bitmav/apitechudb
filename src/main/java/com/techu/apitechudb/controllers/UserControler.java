package com.techu.apitechudb.controllers;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("apitechu/v2")
public class UserControler {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers(@RequestParam(value ="age",defaultValue = "0") int edad) {
        System.out.println("getUsers de User Controler");

        if (edad == 0) {
            System.out.println("Busqueda sin edad");
            return new ResponseEntity<>(
                    userService.findAll(),
                    HttpStatus.OK
            );
        } else {
            System.out.println("Busqueda usuarios de una edad " + edad);

            Optional<List<UserModel>> result;
            result = userService.findByAge(edad);
            return new ResponseEntity<>(
                    result.isPresent() ? result.get() : "No hay usuarios de esa edad",
                    result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
            );
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById de UserControler");
        System.out.println("El id del usuario a consultar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser de UserController");
        System.out.println("La ID del usaurio a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("La id del usuario que se va a actualizar en parametero es " + id);
        System.out.println("La id del usuario que se va a actualizar es " + user.getId());
        System.out.println("El nombre del usuario que se va actualizar es " + user.getName());
        System.out.println("La edad del usuario que se va actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user),HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK      :  HttpStatus.NOT_FOUND
        );
    }
}
