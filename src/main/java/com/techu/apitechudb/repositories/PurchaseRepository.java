package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseItem;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public List<PurchaseModel> findAll(){
        System.out.println("findAll de PurchaseRespository");
        return ApitechudbApplication.purchaseModels;
    }

    public PurchaseModel save(PurchaseModel purchase){
        System.out.println("Save en PurchaseRepository");

        ApitechudbApplication.purchaseModels.add(purchase);

        return purchase;
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById en PurchaseRepository");

        Optional<PurchaseModel> result = Optional.empty();

        for(PurchaseModel purchaseInList: ApitechudbApplication.purchaseModels){
            if (purchaseInList.getId().equals(id)) {
                System.out.println("Compra encontrada");
                result = Optional.of(purchaseInList);
            }
        }

        return result;
    }
}
