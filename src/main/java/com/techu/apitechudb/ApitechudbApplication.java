package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;

	public static ArrayList<UserModel> userModels;

	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);
		ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
		ApitechudbApplication.userModels = ApitechudbApplication.getTestDataUser();
		ApitechudbApplication.purchaseModels = ApitechudbApplication.getTestPurchases();
	}

	private static ArrayList<ProductModel> getTestData(){

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1",
						"Producto 1",
						10
				)
		);

		productModels.add(
				new ProductModel(
						"2",
						"Producto 2",
						20
				)
		);

		productModels.add(
				new ProductModel(
						"3",
						"Producto 3",
						30
				)
		);

		return productModels;
	}

	private static ArrayList<UserModel> getTestDataUser(){

		ArrayList<UserModel> userModels = new ArrayList<>();

		userModels.add(
				new UserModel(
						"1",
						"Usuario 1",
						10
				)
		);

		userModels.add(
				new UserModel(
						"2",
						"Usuario 2",
						20
				)
		);

		userModels.add(
				new UserModel(
						"3",
						"Usuario 3",
						30
				)
		);

		userModels.add(
				new UserModel(
						"4",
						"Usuario 4",
						40
				)
		);

		userModels.add(
				new UserModel(
						"5",
						"Usuario 5",
						50
				)
		);

		userModels.add(
				new UserModel(
						"6",
						"Usuario 6",
						50
				)
		);
		return userModels;
	}

	private static ArrayList<PurchaseModel> getTestPurchases() {

		ArrayList<PurchaseModel> purchaseModels = new ArrayList<>();
		return purchaseModels;
	}
}
